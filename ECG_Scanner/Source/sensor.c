#include "../Headers/sensor.h"
#include <stdio.h>
#include <stdlib.h>

int getNextData(FILE *file)
{
	int c;
	if (fscanf_s(file, "%d", &c) == EOF) {
		fclose(file);
		return INT_MIN;
	}
	return c; // return sensor value
}

FILE* openfile(const char* filename)
{
	FILE *file;
	errno_t err;
	err = fopen_s(&file, filename, "r");
	printf("The file (%s) was %s.\n", filename, (err == 0) ? "opened" : "not opened");
	return file;
}