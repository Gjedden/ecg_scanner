#include "../Headers/qrs.h"
#include "../Headers/history.h"
#include "../Headers/dataProcessor.h"
#include <stdio.h>
#include <stdlib.h>


QRS_params* initQRS() {
	QRS_params* qrs = malloc(sizeof(QRS_params));
	qrs->NPKF = 0; //Assumes no noise
	qrs->SPKF = 2000; //Lowest value that don't trigger warning
	qrs->RR_LOW = 0; // 180 250 * 60/180 ~ 80
	qrs->RR_HIGH = 0; // <120 bpm 250 * 60/120 = 125
	qrs->RR_MISS = 0; // >30bpm 250 * 60/30 = 500
	qrs->RR_Successive_Misses = 0;
	updateThresholds(qrs);

	struct History* peaks = initHistoryArray(500 * 2, 1);
	qrs->peaks = peaks;

	struct History* lastrPeak = initHistoryArray(2, 1);
	qrs->lastrPeak = lastrPeak;

	//RR-Intervals
	struct History* recentRR = initHistoryArray(8, 1); //Average of intervals needs 8 intervals
	qrs->recentRR = recentRR;
	struct History* recentRR_OK = initHistoryArray(8, 1); //Average of intervals needs 8 intervals
	qrs->recentRR_OK = recentRR_OK;

	return qrs;
}

void updateThresholds(QRS_params *qrs) {
	qrs->THRESHOLD1 = qrs->NPKF + (qrs->SPKF - qrs->NPKF) / 4;
	qrs->THRESHOLD2 = qrs->THRESHOLD1 / 2;
}

void updateNPKF(QRS_params *qrs, int value) {
	qrs->NPKF = value / 8 + (qrs->NPKF * 7) / 8;
	updateThresholds(qrs);

}

void rrOK(QRS_params *qrs, int rr, int index, int value) {
	qrs->RR_Successive_Misses = 0; //Good R - Peak, back to 0
	setLastrPeak(qrs,index,value);
	
	//Update moving SPKF, peak amplitude
	qrs->SPKF = value / 8 + qrs->SPKF * 7 / 8;

	//Store RR - interval
	addElement(qrs->recentRR_OK, rr);
	addElement(qrs->recentRR, rr);

	//Update averages
	qrs->RR_AVERAGE2 = historyAverage(qrs->recentRR_OK);
	qrs->RR_AVERAGE1 = historyAverage(qrs->recentRR);

	//Update RR-LOW, RR-High and RR-Miss
	updateRRIntervals(qrs, qrs->RR_AVERAGE2);

	updateThresholds(qrs);
	updateUI(qrs);
}

void searchbackPeakFound(QRS_params *qrs, int rr, int index, int value) {
	qrs->RR_Successive_Misses++; // Peak found using searchback, means missed R - Peak, increment
	setLastrPeak(qrs, index, value);

	//Update moving SPKF, peak amplitude
	qrs->SPKF = value / 4 + qrs->SPKF * 3 / 4;

	//Don't update RR interval average with ridiculous RR interval
	if (rr < qrs->RR_MISS * 2) {
		//Store RR - interval
		addElement(qrs->recentRR, rr);

		//Update average
		qrs->RR_AVERAGE1 = historyAverage(qrs->recentRR);

		//Update RR-LOW, RR-High and RR-Miss
		updateRRIntervals(qrs, qrs->RR_AVERAGE1);

		updateThresholds(qrs);
	}
	else {
		//D�D
	}
	updateUI(qrs);
}



void rPeakDetection(QRS_params *qrs, int index, int value)
{
	start:  // Label (for goto)

	if (value > qrs->THRESHOLD1) // !! Peak > THRESHOLD1? !! //
	{
		int rrInterval = index - getElement(qrs->lastrPeak, 1);  // !! Calculatae RR !! //

		//Check if RR-HIGH has been given a value yet (otherwise we set an initial value)
		if (qrs->RR_HIGH == 0) {
			//This is the first RPeak, we can't talk about intervals yet
			setLastrPeak(qrs, index, value);

			//Give wide values to RR-Low RR-High and RR-Miss as we have no idea about the heartrate yet.
			qrs->RR_LOW = 80;   // 180 bpm: 250 * 60/180 ~= 80
			qrs->RR_HIGH = 375; // 40 bpm:  250 * 60/40 =  375
			qrs->RR_MISS = 500; // 30 bpm:  250 * 60/30 =  500
			qrs->RR_AVERAGE1 = INT_MIN;  // Checks if RR_AVERAGE1 is equal to INT_MIN in the UI (for meaningful display purposes).
			updateUI(qrs);
		}
		else if (rrInterval > qrs->RR_LOW && rrInterval < qrs->RR_HIGH)  // !! RR_LOW < RR < RR_HIGH? !! //
		{
			rrOK(qrs, rrInterval, index, value);  // Yes-path from 'RR_LOW < RR < RR_HIGH?'
		}
		else if(rrInterval > qrs->RR_MISS)  // !! RR > RR_MISS? !! //
		{
			// !! Search backwards through PEAKS and return peak2 !! //
			int backPeak = 2;
			int found = 0;  // "Boolean"
			int searchBackIndex;
			int searchBackValue;

			while (!((searchBackValue = getElement(qrs->peaks, backPeak)) > qrs->THRESHOLD2)) // !! peaks2 > THRESHOLD2? !! //
			{
				backPeak += 2; // Peaks contain both index, "side-by-side" in the array.
			}

			// Yes-path from 'peaks2 > THRESHOLD2?'
			searchBackIndex = getElement(qrs->peaks, backPeak + 1);  // Gets the index of peak2 from our history of all peaks.

			int searchBackrrInterval = searchBackIndex - getElement(qrs->lastrPeak, 1);
			searchbackPeakFound(qrs, searchBackrrInterval, searchBackIndex, searchBackValue);

			goto start;
		}


	}
	else {
		updateNPKF(qrs, value);
	}
}

void addToPeaks(QRS_params *qrs, int index, int value) {
	addElement(qrs->peaks, index);
	addElement(qrs->peaks, value);
}

void setLastrPeak(QRS_params *qrs, int index,int value) {
	addElement(qrs->lastrPeak, index);
	addElement(qrs->lastrPeak, value);
}

void updateRRIntervals(QRS_params *qrs, int rrAverage) {
	qrs->RR_LOW = rrAverage * 92 / 100;
	qrs->RR_HIGH = rrAverage * 116 / 100;
	qrs->RR_MISS = rrAverage * 166 / 100;
}

void updateUI(QRS_params *qrs) {
//#if defined(__linux__)
//	system("clear");
//#else
//#if defined(_WIN32)
//	system("cls");
//#endif
//#endif

	//system("cls");
	printf(" R-Peak Time-index: %d \n", getElement(qrs->lastrPeak,1));
	printf(" R-Peak Value:      %d \n", getElement(qrs->lastrPeak, 0));
	if(getElement(qrs->lastrPeak, 0) < 2000) {
		printf(" Warning, R - Peak value below  2000!! \n");
	}
	if (qrs->RR_Successive_Misses >= 5) {
		printf(" Warning, Irregular heartbeat!!\n %d successive heartbeats missed a normal interval\n", qrs->RR_Successive_Misses);
	}
	if (qrs->RR_AVERAGE1 == INT_MIN) {
		printf("\n BPM: N/A \n"); // 250*60 = 15000   -   250 samples/s, 60s/m
	}
	else {
		printf("\n BPM: %d \n", 15000 / qrs->RR_AVERAGE1); // 250*60 = 15000   -   250 samples/s, 60s/m
	}

	//printf(" Threshold:      %d \n", qrs->THRESHOLD1);
	printf("\n/////////////////////////////////////////////// \n \n");
}