#include "../Headers/main.h"
#include "../Headers/dataProcessor.h"
#include <stdio.h>
#include <stdlib.h>

// Main function for organizing the program execution.
// The functions and object predefined are just for inspiration.
// Please change orden,names arguments to fit your solution.

int main()
{	
	startProcessing();
	return 0;
}
