#include "../Headers/filters.h"
#include "../Headers/dataProcessor.h"
#include "../Headers/history.h"

struct FilterHistory* initFilters() {
	struct FilterHistory* filterhistory = malloc(sizeof(struct FilterHistory));
	filterhistory->rawDataHistory = initHistoryArray(13, 1);	// sensor values
	filterhistory->lowPassHistory = initHistoryArray(33, 1); // filtered sensor values
	filterhistory->lowPassHistory2 = initHistoryArray(33, 1); // used to restart low pass filter and avoid overflows
	filterhistory->lowPassHistory2Count = INT_MIN;
	filterhistory->highPassHistory = initHistoryArray(5, 1); // filtered low pass values
	filterhistory->derivativeHistory = initHistoryArray(1, 1);	// filtered high pass values
	filterhistory->squaringHistory = initHistoryArray(30, 1); // filtered derivative filters
	filterhistory->filteredData = initHistoryArray(6, 0); // final filtered values

	return filterhistory;
}

void filterSample(struct FilterHistory* filterHistory, int sample) {
	addElement(filterHistory->rawDataHistory, sample);
	lowPassFilter(filterHistory->rawDataHistory, filterHistory->lowPassHistory);
	if (filterHistory->lowPassHistory2Count == INT_MIN){
		if (abs(getElement(filterHistory->lowPassHistory, 0)) > 10000){
		filterHistory->lowPassHistory2Count = 0;
		filterHistory->lowPassHistory2 = initHistoryArray(33, 1); // used to restart low pass filter and avoid overflows
		}
	}
	else {
		lowPassFilter(filterHistory->rawDataHistory, filterHistory->lowPassHistory2);
		filterHistory->lowPassHistory2Count ++;
	}
	if (filterHistory->lowPassHistory2Count >= 100) {
		filterHistory->lowPassHistory2Count = INT_MIN;
		destroyHistory(filterHistory->lowPassHistory);
		filterHistory->lowPassHistory = filterHistory->lowPassHistory2;
	}
	highPassFilter(filterHistory->lowPassHistory, filterHistory->highPassHistory);
	derivativeFilter(filterHistory->highPassHistory, filterHistory->derivativeHistory);
	squaringFilter(filterHistory->derivativeHistory, filterHistory->squaringHistory);
	mwiFilter(filterHistory->squaringHistory, filterHistory->filteredData);
}

int getFilteredElement(struct FilterHistory* filterHistory, int index) {
	return getElement(filterHistory->filteredData, index);
}

void lowPassFilter(struct History *valHist, struct History *lpHist)
{
	int y = 2*getElement(lpHist,0) - getElement(lpHist,1) + (getElement(valHist,0) - 2*getElement(valHist,6) + getElement(valHist,12))/32;
	addElement(lpHist, y);
}

void highPassFilter(struct History *valHist, struct History *hpHist)
{
	int y = getElement(hpHist,0) - getElement(valHist,0) / 32 + getElement(valHist,16) - getElement(valHist,17) + getElement(valHist,32) / 32;
	addElement(hpHist, y);
}


void derivativeFilter(struct History *valHist, struct History *drvHist)
{
	int y = (2*getElement(valHist,0) + getElement(valHist,1) - getElement(valHist,3) - 2 * getElement(valHist,4)) / 8;
	addElement(drvHist, y);
}

void squaringFilter(struct History *valHist, struct History *sqrHist)
{
	int y = getElement(valHist, 0);
	y *= y;
	addElement(sqrHist, y);
}

void mwiFilter(struct History *valHist, struct History *mwiHist)
{
	int y = 0;
	for (int i = 0; i < 30; i++)
	{
		y += getElement(valHist, i) / 30;
	}
	addElement(mwiHist, y);
}

