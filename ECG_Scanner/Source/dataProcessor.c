#include "../Headers/sensor.h"
#include "../Headers/filters.h"
#include "../Headers/qrs.h"
#include "../Headers/dataProcessor.h"
#include <stdio.h>
#include <stdlib.h>

void peakCorrection(struct History* filterHistory, int offsetAmount, int* offsetIndex, int* offsetValue, int currentIndex, int currentValue) {
	//Find peak in that interval
	for (int j = offsetAmount - 1; j >= 0; j--) {
		if (getFilteredElement(filterHistory, j) > *offsetValue) {
			*offsetValue = getFilteredElement(filterHistory, j);
			*offsetIndex = currentIndex - j;
		}
	}
}

void startProcessing()
{
	QRS_params* qrs_params = initQRS();  // Initializes the QRS_params struct and allocates memory for contained arrays and structs.
	struct FilterHistory* filterHistory = initFilters();  // Initializes a struct containing histories of all the filters.

	FILE *file;  // Pointer to a file object
	file = openfile("ECG.txt");
	int sample;  // The Integer that will temporarily contain all future samples from our "sensor".

	int i = 1;
	int rising = 0;  // "Boolean", 
	int offset = 5, offsetValue = 0;  // Used for peak correction.
	int requiredSamples = 32 + offset;  // The required samples before we can start processing.
	int current = 0;  // The last filtered value

	while ((sample = getNextData(file)) != INT_MIN)
	{
		//Filter the new data point
		filterSample(filterHistory, sample);  // What are we actually doing here? Why are we not just saving the samples? We're also processing the LowPassFilter here.

		//Current and recent sample
		offsetValue = getFilteredElement(filterHistory, offset);
		current = getFilteredElement(filterHistory, 0);  // Gets the last filtered value

		
		if (i > requiredSamples)  //Only process when we have enough data for all filters to be meaningfull.
		{
			if (current > offsetValue) rising = 1;

			//Is sample a peak
			else if (rising && (offsetValue > current))  // The values have been rising, and are now (on average over 5 sample points) falling again.
			{
				//Peak is between current and offset
				int maxIndex = i - offset;
				int maxValue = offsetValue;
				// !! Find peak !! //
				peakCorrection(filterHistory, offset, &maxIndex, &maxValue, i, current);  // Finds the actual peak and sets maxIndex and maxValue to the values of that peak.
				
				// !! Store Peak in PEAKS !! //
				addToPeaks(qrs_params, maxIndex, maxValue);

				rPeakDetection(qrs_params, maxIndex, maxValue);  // Sends the relevant info to finish the rest of the QRS algo.
				rising = 0;
			}
		}
		
		i++;
		Sleep(4);
	}
}