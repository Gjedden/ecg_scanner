#include <stdio.h>
#include <stdlib.h>
#include "../Headers/history.h"

struct History* initHistoryArray(int size, int fill) {
	struct History* history = malloc(sizeof(struct History));
	history->size = size;
	history->array = malloc(size * sizeof(int));
	history->startIndex = size - 1;
	if (fill != 0) {
		for (int i = 0; i < size; i += 1) {
			history->array[i] = 0;
		}
	}
	return history;
}

void destroyHistory(struct History *history) {
	free(history->array);
	free(history);
}

void addElement(struct History *history, int data)
{
	history->startIndex -= 1;
	if (history->startIndex < 0) {
		history->startIndex += history->size;
	}
	history->array[history->startIndex] = data;
}

int getElement(struct History *history, int index) {
	return history->array[(history->startIndex + index) % history->size];
}

int historyAverage(struct History *history) {

	int n = 0;
	int tally = 0;
	for (int i = 0; i < history->size; i++) {
		//Discard elements that are 0, we assume it means that the history has not yet been filled with these values
		//as the history array is initialised with all zeros in relevant cases
		if (getElement(history, i) == 0) {
			break;
		}
		tally += getElement(history, i);
		n++;
	}
	if (n == 0) {
		return INT_MIN;
	}
	return tally / n;
}
