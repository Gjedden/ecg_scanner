#pragma once
#include <stdio.h>
#include <stdlib.h>
struct History {
	int  size;
	int  *array;
	int  startIndex;
};
struct History* initHistoryArray(int size, int fill);
void destroyHistory(struct History *history);
void addElement(struct History *history, int data);
int getElement(struct History *history, int index);

int historyAverage(struct History *history);