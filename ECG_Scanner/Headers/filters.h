#ifndef FILTERS_H
#define FILTERS_H
#include "dataProcessor.h"
#include "../Headers/history.h"
struct FilterHistory
{ // Structure for Filter histories
	struct History* rawDataHistory;
	struct History* lowPassHistory;
	struct History* lowPassHistory2;
	int lowPassHistory2Count;
	struct History* highPassHistory;
	struct History* derivativeHistory;
	struct History* squaringHistory;
	struct History* filteredData;
};

void lowPassFilter(struct History *valHist, struct History *lpHist);
void highPassFilter(struct History *valHist, struct History *hpHist);
void derivativeFilter(struct History *valHist, struct History *drvHist);
void squaringFilter(struct History *valHist, struct History *sqrHist);
void mwiFilter(struct History *valHist, struct History *mwiHist);
struct FilterHistory* initFilters();
void filterSample(struct FilterHistory* filterHistory, int sample);
int getFilteredElement(struct FilterHistory* filterHistory, int index);
#endif // FILTERS_H