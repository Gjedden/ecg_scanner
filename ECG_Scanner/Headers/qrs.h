#ifndef QRS_H
#define QRS_H
#include "dataProcessor.h"
#include "../Headers/history.h"

// Header file for QRS functionality 
// it is recommended to use the structure "QRS_parameters"
// to organize all variables and parameters

typedef struct QRS_params
{ // Structure for QRS parameters
   int SPKF;
   int NPKF; 
   int THRESHOLD1;
   int THRESHOLD2;

   int RR_AVERAGE1;
   int RR_AVERAGE2;
   int RR_LOW;
   int RR_HIGH;
   int RR_MISS;
   int RR_Successive_Misses;

   struct History *peaks;
   struct History *lastrPeak;
   struct History *recentRR;
   struct History *recentRR_OK;
} QRS_params;

// Feel free to change and add methods

void rrOK(QRS_params *qrs, int rr, int index,int value);

void rPeakDetection(QRS_params *qrs, int index, int value);

void updateRRIntervals(QRS_params *qrs, int rrAverage);

QRS_params* initQRS();

void updateThresholds(QRS_params *qrs);

void addToPeaks(QRS_params *qrs, int index, int value);
void setLastrPeak(QRS_params *qrs, int index, int value);
void updateUI(QRS_params *qrs);
#endif // QRS_H
